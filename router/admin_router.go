package router

import (
	apiController "gin-product/controllers/api"
	"gin-product/middleware"

	"github.com/gin-gonic/gin"
)

func SetupAdminRouter(router *gin.Engine) {
	productController := &apiController.ProductController{}
	// create group
	api_v1 := router.Group("/api/v1")
	{
		admin := api_v1.Group("/admin")
		admin.Use(middleware.JWTMiddleware())
		{
			admin.GET("/token-decode", productController.DecodeJWTToken)
		}
		products := admin.Group("/products")
		{
			products.GET("", productController.Index)
			products.GET(":id", productController.Show)
		}
	}

}
