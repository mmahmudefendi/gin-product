package router

import (
	apiController "gin-product/controllers/api"

	"github.com/gin-gonic/gin"
)

func SetupAPIRouter(router *gin.Engine) {
	productController := &apiController.ProductController{}
	// create group
	api_v1 := router.Group("/api/v1")
	{
		products := api_v1.Group("/products")
		{
			products.GET("", productController.Index)
			products.GET(":id", productController.Show)
		}
	}
}
