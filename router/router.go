package router

import "github.com/gin-gonic/gin"

func SetupRouter() *gin.Engine {
	router := gin.Default()

	SetupAdminRouter(router)
	SetupAPIRouter(router)

	return router
}
