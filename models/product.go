package models

import (
	"gin-product/database"
	"time"

	"gorm.io/gorm"
)

var DB *gorm.DB = database.DB

/*
Product is Belong To Store
One Store can have many products
One Product can only have one Store
One Product can only have one Categories
*/

// Product model
type Product struct {
	// gorm.Model
	ID         uint       `gorm:"primary_key" json:"id"`
	Name       string     `json:"name"`
	Created_At time.Time  `gorm:"default:CURRENT_TIMESTAMP()" json:"created_at"`
	Updated_At time.Time  `gorm:"default:CURRENT_TIMESTAMP()" json:"updated_at"`
	Deleted_At *time.Time `sql:"index" json:"deleted_at"`
}
