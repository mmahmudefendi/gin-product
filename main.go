package main

import (
	"gin-product/database"
	"gin-product/router"
	"log"
	"os"

	"github.com/joho/godotenv"
)

func main() {
	db, err := database.SetupDatabase()
	if err != nil {
		log.Fatal("Error setting up database:", err)
	}
	defer func() {
		sqlDB, err := db.DB()
		if err != nil {
			log.Fatal("Error getting underlying *sql.DB:", err)
		}
		err = sqlDB.Close()
		if err != nil {
			log.Fatal("Error closing database:", err)
		}
	}()

	router := router.SetupRouter()
	err = godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file:", err)
	}
	router.Run(os.Getenv("PORT"))
}
