package controller

import (
	"fmt"
	"gin-product/models"
	"log"
	"net/http"
	"os"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt"
	"gorm.io/gorm"
)

type Controller struct{}

func (c *Controller) SetResult(data interface{}, err error) (gin.H, int) {
	if err != nil {
		return gin.H{
			"status":  "error",
			"header":  "failed!",
			"message": "There is a mistake",
			"data":    err.Error(),
		}, 400
	}
	return gin.H{
		"status":  "success",
		"header":  "congratulation!",
		"message": "",
		"data":    data,
	}, 200
}

func (c *Controller) SetDataTables(request *http.Request, query *gorm.DB, total int) (gin.H, error) {
	totalFiltered := 0
	offset := 0
	perPage := 10
	sortBy := "id"
	sortDir := "desc"

	start := request.FormValue("start")
	if start != "" {
		startInt, err := strconv.Atoi(start)
		println("startInt : ", startInt)
		if err != nil {
			return nil, err
		}
		offset = startInt
	}

	length := request.FormValue("length")
	if length != "" {
		lengthInt, err := strconv.Atoi(length)
		println("lengthInt : ", lengthInt)
		if err != nil {
			return nil, err
		}
		perPage = lengthInt
	}

	orderColumn := request.FormValue("order[0][column]")
	if orderColumn != "" {
		orderColumnInt, err := strconv.Atoi(orderColumn)
		if err != nil {
			return nil, err
		}
		columnData := request.FormValue(fmt.Sprintf("columns[%d][data]", orderColumnInt))
		if columnData != "" {
			sortBy = columnData
		}
	}

	orderDir := request.FormValue("order[0][dir]")
	if orderDir != "" {
		sortDir = orderDir
	}

	var count int64
	if err := query.Count(&count).Error; err != nil {
		return nil, err
	}
	totalFiltered = int(count)

	query = query.Order(fmt.Sprintf("%s %s", sortBy, sortDir))
	var data []models.Product
	if err := query.Offset(offset).Limit(perPage).Find(&data).Error; err != nil {
		return nil, err
	}

	results := gin.H{
		"draw":            request.FormValue("draw"),
		"data":            data,
		"recordsTotal":    total,
		"recordsFiltered": totalFiltered,
	}

	return results, nil
}

func (c *Controller) DecodeJWTToken(tokenString string) (jwt.MapClaims, error) {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		// Pastikan Anda memiliki kunci rahasia yang sama yang digunakan untuk menghasilkan token
		// Di sini, contoh menggunakan kunci tipe []byte, tetapi Anda dapat menyesuaikannya sesuai dengan kebutuhan Anda
		secretKey := []byte(os.Getenv("JWT_SECRET"))

		// Validasi metode tanda tangan token
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		return secretKey, nil
	})

	if err != nil {
		log.Println("Error parsing JWT token:", err) // Cetak pesan kesalahan untuk debugging
		return nil, err
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok || !token.Valid {
		log.Println("Invalid token") // Cetak pesan kesalahan untuk debugging
		return nil, fmt.Errorf("Invalid token")
	}

	return claims, nil
}
