package api

import (
	"fmt"
	controller "gin-product/controllers"
	"gin-product/database"
	"gin-product/models"
	"log"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
)

type ProductController struct{}

var globalController = &controller.Controller{}

func (pc *ProductController) Index(c *gin.Context) {
	var products []models.Product
	err := database.DB.Find(&products).Error

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": "Internal Server Error",
		})
		return
	}

	query := database.DB.Model(&models.Product{})
	searchValue := c.Request.FormValue("search[value]")
	if searchValue != "" {
		query = query.Where("name LIKE ?", "%"+searchValue+"%")
	}

	results, err := globalController.SetDataTables(c.Request, query, len(products))
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": "Internal Server Error",
		})
		return
	}

	c.JSON(http.StatusOK, results)
}

func (pc *ProductController) Show(c *gin.Context) {
	id := c.Param("id")

	var products models.Product
	err := database.DB.First(&products, id).Error
	if err != nil {
		c.JSON(404, gin.H{"error": "Product not found"})
		return
	}

	responseData, statusCode := globalController.SetResult(products, err)
	c.JSON(statusCode, responseData)
}

func (pc *ProductController) DecodeJWTToken(c *gin.Context) {
	tokenString := c.GetHeader("Authorization")
	fmt.Println("Token String:", tokenString) // Cetak tokenString untuk debugging

	// Hapus "Bearer " dari tokenString
	tokenString = strings.Replace(tokenString, "Bearer ", "", 1)

	claims, err := globalController.DecodeJWTToken(tokenString)
	if err != nil {
		// Tangani kesalahan jika terjadi
		log.Println("Error decoding JWT token:", err) // Cetak pesan kesalahan untuk debugging
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": "Failed to decode JWT token",
		})
		return
	}

	// Lakukan sesuatu dengan nilai-nilai dalam klaim token
	usrClaim, ok := claims["usr"].(map[string]interface{})
	if !ok {
		// Tangani kesalahan jika nilai "usr" tidak valid
		log.Println("Invalid 'usr' claim") // Cetak pesan kesalahan untuk debugging
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": "Invalid 'usr' claim",
		})
		return
	}

	email, ok := usrClaim["email"].(string)
	if !ok {
		// Tangani kesalahan jika nilai "email" tidak valid
		log.Println("Invalid 'email' claim") // Cetak pesan kesalahan untuk debugging
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": "Invalid 'email' claim",
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"email": email,
		// Tambahkan data lain yang perlu ditampilkan
	})
}
