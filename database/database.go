package database

import (
	_ "github.com/go-sql-driver/mysql"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var DB *gorm.DB

func SetupDatabase() (*gorm.DB, error) {
	dsn := "root:@tcp(localhost:3000)/go_products?charset=utf8&parseTime=true"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		return nil, err
	}

	// AutoMigrate model-model Anda di sini
	// db.AutoMigrate(&models.Product{})
	DB = db
	return db, nil
}
